int	bool
1	true
0	false
-1	true


str	bool
"text"	true
""	false
"123"	true

�������� � Python3:

>>> n=-1
>>> type(n)
<class 'int'>		#�������� ���
>>> bool(n)
True
>>> n=-1
>>> type(n)
<class 'int'>
>>> bool(n)
True
>>> n=0
>>> type(n)
<class 'int'>
>>> bool(n)
False
>>> bool("�����")
True
>>> bool("�����_�����")
True
>>> bool("")
False

Bool(str) ������ true ���� str �� null
Bool(int) true ���� int �� ����� ����, ���� ��� ������������� ��������� int.

