"""
Напишите программу, которая читает данные из файлов
/etc/passwd и /etc/group на вашей системе и выводит
следующую информацию в файл output.txt:
1. Количество пользователей, использующих все имеющиеся
интерпретаторы-оболочки.
( /bin/bash - 8 ; /bin/false - 11 ; ... )
2. Для всех групп в системе - UIDы пользователей
состоящих в этих группах.
( root:1, sudo:1001,1002,1003, ...)

"""
f1 = open('passwd','r')
f2 = open('group','r')
fout = open('output.txt','w')

lines = f1.readlines()
lines2 = f2.readlines()

f1.close()
f2.close()

shells = []
uids = {}
for i in range(lines.__len__()):
	s = lines[i].split(':')
	shells.append(s[s.__len__()-1].rstrip("\n"))
	uids[s[0]] = s[2]

shells.sort()

ff = ''
j = 0

for i in range(shells.__len__()):
	if ff != shells[i]:
		if ff!='':
			buffer = str(ff) + ' - ' + str(j) + " ; "
			fout.write(buffer)
		ff = shells[i]
		j = 1
	else:
		j = j + 1

fout.write('\n')

for i in range(lines2.__len__()):
	s = lines2[i].split(':')
	if s.__len__() > 3:
		if s[3] != '\n':
			buffer = s[0] + ":"
			fout.write(buffer)
			for j in range(3, s.__len__()):
				st = s[j].rstrip('\n')
				ss = st.split(',')
				for k in ss:
					if k != '':
						buffer = uids[k] + ","
						fout.write(buffer)
			fout.write(" ")
fout.close()