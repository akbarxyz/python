"""
The decimal number, 585 = 10010010012 (binary), is palindromic in both bases.

Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.

(Please note that the palindromic number, in either base, may not include leading zeros.)

"""
result = 0
for i in range(1000000):
	base10 = str(i)
	base2 = i.__format__('b')
		
	base10_is_palindrom = True
	base2_is_palindrom = True
		
	base10_length = int(base10.__len__() / 2)
	base2_length = int(base2.__len__() / 2)
	
	# проверка десятичного числа, является ли оно палиндромом
	for n in range(base10_length):
		if ( base10[n] ) != ( base10[(base10.__len__())-n-1] ):
			base10_is_palindrom = False
	
	# проверка двоичного числа, является ли оно палиндромом
	for m in range(base2_length):
		if ( base2[m] ) != ( base2[(base2.__len__())-m-1] ):
			base2_is_palindrom = False
		
	if base10_is_palindrom == True and base2_is_palindrom == True:
	#	print(base10, " -> ", i.__format__('b')) Печатает на экран найденные палиндромы
		result += i
	
print('The sum of the palindromic numbers in base10 and base2 is: ', result)